% CIF2HKL(1)
% Emmanuel Farhi
% December 2024

# NAME

**cif2hkl** - Read a CIF/CFL/SHX/PCR crystallographic description and generates a HKL F^2 reflection list.

# SYNOPSIS

**cif2hkl** [-hpvx][-l LAMBDA][-i FILE][-m MODE][-o OUTFILE] `CIF_FILE`

# DESCRIPTION

**cif2hkl** is a program that computes structure factors |F|^2 for neutrons, x-rays, and electrons. 
It reads a file with crystal structure information, and computes the |F|^2 structure factors. 
The supported input file formats are CIF PCR (FullProf) CFL (CrysFML) SHX/INS/RES (ShelX).

The result is a list of structure factor intensity |F|^2. 
The output format contains a clear text header with metadata, and the reflection
list can be used with e.g. McXtrace and McStas.
  
# OPTIONS

**-h**
:   Show this help

**-l LAMBDA**
:   Set the incoming probe wavelength (in Angs). Default is 0.5

**-m MODE**
:   Generate structure factors for given probe, where MODE is
    NUC=neutron(default) XRA=xrays ELE=electrons

**-o OUTPUT**
:   Use given name OUTPUT for the generated file.

**-p**
:   Generate a list of unique HKL reflections (for powders). Default.

**-x**
:   Generate a list of all HKL reflections (for single crystals).

**CIF_FILE** 
:   Input file in CIF, PCR, CFL, SHX, INS, RES format.

# EXAMPLES

Compute F2 for CaF2 crystal
:     cif2hkl --powder --mode NUC -o CaF2.laz CaF2.cfl

# AUTHORS

Emmanuel Farhi (emmanuel.farhi@synchrotron-soleil.fr). 
https://gitlab.com/soleil-data-treatment/soleil-software-projects/cif2hkl

